#ifndef _H_COMUNICACION_
#define _H_COMUNICACION_

#include <string>
using namespace std;




#include <time.h>
#include <sys/socket.h>
#include <arpa/inet.h> 
#include <unistd.h>
#include <cstring>

#include "config.hpp"
#include "Serializable.hpp"
#include "Carta.hpp"

class Comunicacion{

    private:
        int socket;


    public:
        Comunicacion(int socket);


        int recibirInt();


        void enviar(int cod);

        //mensajes para enviar

        void enviarCodigoConexionAceptada();


        void enviarCodigoErrorMaximoClientes();

        void enviarCodigoTerminar();

        void enviarCodigoErrorApuesta();

        void enviarCodigoPreguntarApuestas();

        void enviarCodigoConfirmarApuesta();

        void enviarCodigoPerdirPlantar();



        void enviar(const string &s);

        string recibirString();


        void enviar(const Carta &s);

        //template <typename T>
        //T recibirSerializable()
        //{
            //T obj;

            //obj.desserializar(this->recibirString());


            //return obj;
        //}


        Carta recibirCarta();
};




#endif /* end of include guard: _H_COMUNICACION_ */
