#ifndef _H_SERIALIZABLE_
#define _H_SERIALIZABLE_

#include <vector>
#include <string>
#include <sstream>
using namespace std;



class Serializable{
    protected:


        vector<string> split(char c, const string &recv) const;
        vector<string> parser(const string &recv) const;


        template <typename T>
        string toFormat(const vector<T> &vec) const
        {
            string s;

            s.push_back('|');

            for(T x: vec)
            {
                stringstream ss;

                ss << x;

                s.append(ss.str());
                s.push_back('|');
            }

            return s;
        }

    public:

        virtual string serializar() const = 0;
        virtual void desserializar(const string &s) = 0;

};



#endif /* end of include guard: _H_SERIALIZABLE_ */
