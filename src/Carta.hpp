#ifndef _H_CARTA_
#define _H_CARTA_


#include <string>
using namespace std;


#include "Serializable.hpp"


enum class Palo{
    Pica,
    Corazones,
    Diamantes,
    Treboles
};




class Carta: public Serializable{

    public:
        int numero;
        Palo palo;

        Carta(int numero, Palo palo);
        Carta();


        string toString() const;

        bool operator==(const Carta &c);

        string serializar() const;
        void desserializar(const string &s);
};


#endif /* end of include guard: _H_CARTA_ */
