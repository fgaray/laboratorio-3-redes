#include <iostream>
using namespace std;

#include <time.h>
#include <sys/socket.h>
#include <arpa/inet.h> 
#include <unistd.h>
#include <pthread.h> 
#include <cstring>


#include "Servidor.hpp"
#include "Carta.hpp"
#include "Estado.hpp"
#include "Comunicacion.hpp"

#include "config.hpp"

Estado *estado;

int main(int argc, const char *argv[])
{

    srand(time(nullptr));

    int socket_desc, client_sock, c;
    struct sockaddr_in server , client;

    unsigned int cantidad_clientes = 0;

    //creamos un socket
    socket_desc = socket(AF_INET , SOCK_STREAM , 0);

    if(socket_desc == -1)
    {
        cerr << "Could not create socket" << endl;
        return -1;
    }

    cout << "Socket created" << endl;

    //Prepare the sockaddr_in structure
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons(PORT);

    if(bind(socket_desc,(struct sockaddr *)&server , sizeof(server)) < 0)
    {
        cerr << "No se pueden escuchar en el puerto" << endl;
        return -1;
    }

    //escuchamos en el socket
    //La lista de pendientes va a ser 3
    listen(socket_desc , 3);

    cout << "Esperando a conexiones entrantes en el puerto " << PORT << endl;

    pthread_t thread_id;

    estado = new Estado;

    pthread_create(&thread_id, NULL, funcion_banca, nullptr);


    while((client_sock = accept(socket_desc, (struct sockaddr *)&client, (socklen_t*)&c)) )
    {
        cantidad_clientes++;

        if(cantidad_clientes > MAX_CLIENTES)
        {
            cout << "No se pudo servir la conexcion: Limite de clientes alcanzado" << endl;

            //write(client_sock, ERROR_MAXIMO_CLIENTES, TAMANO_BYTES_CODIGOS);

            close(client_sock);

            cantidad_clientes--;

        }else{
            cout << "Conexion aceptada" << endl;

            int *socket_privado = new int;

            *socket_privado = client_sock;

            if(pthread_create( &thread_id, NULL, funcion_cliente, (void*)socket_privado) < 0)
            {
                cerr << "No se puede crear el hilo" << endl;
                return -1;
            }


            cout << "Listo, conexion manejada por un hilo" << endl;
        }
    }

    return 0;
}


void *funcion_cliente(void *args)
{
    int sock = *(static_cast<int*>(args));


    Comunicacion cliente(sock);

    Jugador numeroJugador = estado->agregarJugador(sock);

    cliente.recibirInt();


    bool plantado = false;

    bool primera = true;

    while(!plantado)
    {
        //preguntar apuestas

        if(primera)
        {
            cliente.enviarCodigoPreguntarApuestas();

            //nos va a enviar el numero que quiere apostar

            int apuesta = cliente.recibirInt();

            cout << "Apuesta " << apuesta << endl;

            while(!estado->apostar(numeroJugador, apuesta))
            {
                //fallamos, debemos volvera preguntar por la apuesta

                cliente.enviarCodigoErrorApuesta();

                //obtenemos nuevamente el valor de la apuesta
                int apuesta = cliente.recibirInt();
            }

            //ok, enviamos condigo de condirmacion
            cliente.enviarCodigoConfirmarApuesta();

            primera = false;
        }

        //ahora debemos pedir si se quiere plantar o no, PERO ANTES! debemos
        //esperar a que se hayan enviado las cartas

        while(!estado->cartasEnviadas())
        {

        }


        cliente.enviarCodigoPerdirPlantar();

        switch(cliente.recibirInt())
        {
            case CODIGO_PLANTARSE:
                estado->plantar(numeroJugador);
                plantado = true;
                break;

            default:
                estado->notificarRespuesta(numeroJugador);
                break;
        }


        while(!estado->todosListos())
        {

        }

        cout << "[DEBUG] Continamos" << endl;
    }


    //el jugador esta plantado
    //Ahora solo debemos enviarle notificaciones

    while(!estado->todosPlantados())
    {

    }


    return nullptr;
}



void *funcion_banca(void *args)
{
    estado->esperarJugadores();

    cout << "[BANCA] Hay dos jugadores conectados, deben enviarme las apuestas" << endl;

    estado->broadcast(CONEXION_ACEPTADA);

    //inicial

    estado->esperarApuestas();

    //ok, listos con las apuestas

    
    cout << "[BANCA] Ahora reparto las cartas iniciales" << endl;

    estado->repartirCartasIniciales();

    estado->broadcast(ENVIAR_CARTAS_INICIALES);

    cout << "[BANCA] Envio las cartas a los otros" << endl;

    estado->enviarCartasIniciales();

    estado->sacarCartaBanca();

    //enviar cartas a los jugadores respectivos

    cout << "[BANCA] Espero a que los jugadores envien su comando sobre si se plantan o no" << endl;

    //deben enviar comando si plantarse o continuar
    while(!estado->todosListos())
    {

    }

    //si no estan todos planteador hay seguir repartiendo cartas
    while(!estado->todosPlantados())
    {
        cout << "[BANCA] Ahora reparto las cartas" << endl;

        estado->repartirCarta();
        estado->broadcast(ENVIAR_CARTAS);
        estado->enviarUltimaCarta();

        estado->sacarCartaBanca();

        while(!estado->todosListos())
        {

        }

        estado->siguienteRonda();
    }

    //enviamos el resultado
} 
