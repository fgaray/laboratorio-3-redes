#include "Serializable.hpp"



vector<string> Serializable::parser(const string &recv) const
{
    return this->split('|', recv);
}


#include <iostream>



vector<string> Serializable::split(char c, const string &recv) const
{
    string accum;
    vector<string> vars;

    for(char x: recv)
    {
        if(x == c)
        {
            vars.push_back(accum);
            accum.erase();
        }else{
            accum.push_back(x);
        }
        
    }

    vars.erase(vars.begin());

    return vars;
}


