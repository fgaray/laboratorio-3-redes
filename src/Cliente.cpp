#include <iostream>
using namespace std;

#include <time.h>
#include <sys/socket.h>
#include <arpa/inet.h> 
#include <unistd.h>
#include <pthread.h> 
#include <cstring>

#include <stdio.h>


#include "Carta.hpp"
#include "Estado.hpp"
#include "Comunicacion.hpp"
#include "Serializable.hpp"
#include "config.hpp"

#include "Cliente.hpp"



int main(int argc, const char *argv[])
{
    
    int estado = 0;

	char *ip = (char *)malloc(10*sizeof(char));
	int c_socket; 
	struct sockaddr_in cli_info;
	bool quiera_jugar = true;
	bool fin_partida = false;

	cout << "Ingrese la direccion IP del servidor: " << endl;

	cin >> ip;

	//se crea el socket
	c_socket = socket(AF_INET, SOCK_STREAM, 0);

	//verificamos que se haya creado bien el socket
	if (c_socket == -1)
	{
    		cerr << "No se pudo crear el socket" << endl;
	        return -1;
	}

    vector<Carta> cartas;

	cli_info.sin_family = AF_INET;
	cli_info.sin_addr.s_addr = inet_addr(ip); //capta ip servidor
	cli_info.sin_port = htons(PORT); //el puerto a usar

	//Intentamos conectarnos al servidor

	if (connect(c_socket, (struct sockaddr *)&cli_info, sizeof(cli_info) ) == -1)
	{
    		cerr << "No se pudo establecer la conexión" << endl;
	        return -1;
	}

    int fichas = 100;

	//ya estamos conectados

    Comunicacion servidor(c_socket);

    cout << "Conexion establecida con exito" << endl;
    cout << "Esperando a los otros jugadores..." << endl;
	
    int x = servidor.recibirInt();

    if(x == CONEXION_ACEPTADA)
    {
        cout << "LISTOS" << endl;

        servidor.enviar(CONTINUAR);

        //ok, ahora debemos ver que nos pide el servidor

        bool plantado = false;

        while(!plantado)
        {
            int cmd = servidor.recibirInt();

            if(estado == 1 and cmd != ENVIAR_CARTAS)
            {
                continue;
            }

            switch(cmd)
            {
                case PREGUNTAR_APUESTAS: {
                    //ok, debemos pedir la apuesta

                    cout << "Cuando quiere apostar?, dispone de:  " << fichas << ": " << endl;

                    int apuesta;

                    cin >> apuesta;

                    while(apuesta > fichas and apuesta > 0)
                    {
                        cout << "Error , usted no tiene esa cantidad de fichas, usted tiene: " << fichas << endl;
                        cout << "Intente otra vez" << endl;
                        cin >> apuesta;
                    }

                    servidor.enviar(apuesta);

                    if(servidor.recibirInt() == ERROR_APUESTA_INVALIDA)
                    {
                        cout << "Error haciendo la apuesta" << endl;
                    }

                    cout << "Apuesta realizada" << endl;
                    cout << "Esperando a los otros jugadores" << endl;

                    cmd = 0;

                    break;
                    
                }
                case PEDIR_PLANTAR: {
                    cout << "Sus cartas son: ";
                    for(auto c: cartas)
                    {
                        cout << c.toString() << " ; ";
                    }
                    cout << endl;

                    cout << "Se quiere plantar? (s/n): ";

                    char respuesta;

                    cin >> respuesta;

                    while(respuesta != 's' and respuesta != 'n')
                    {
                        cout << "Responda con s/n: " << endl;

                        cin >> respuesta;

                        
                    }

                    if(respuesta == 's')
                    {
                        // nos plantamos
                        plantado = true;

                        servidor.enviar(CODIGO_PLANTARSE);
                    }else{
                        servidor.enviar(CODIGO_NO_PLANTARSE);
                    }

                    cout << "Esperando a los otros jugadores" << endl;

                    cmd = 0;

                    estado = 1;

                    break;
                }

                case ENVIAR_CARTAS: {
                    cout << "[DEBUG] Recibo la carta de esta ronda" << endl;
                    Carta c1 = servidor.recibirCarta();
                    cartas.push_back(c1);
                    estado = 2;
                    break;
                }

                case ENVIAR_CARTAS_INICIALES: {
                    cout << "[DEBUG] Recibo las cartas iniciales" << endl;
                    Carta c1 = servidor.recibirCarta();
                    Carta c2 = servidor.recibirCarta();

                    cartas.push_back(c1);
                    cartas.push_back(c2);

                    break;

                }
                default: {
                    cout << "Codigo no valido" << endl;
                    break;
                }
            }
        }
    }else{
        cout << "Error estableciendo la conexion con el servidor: " << x << endl;
    }


    return 0;
}


void mostrar_mano(vector<Carta> mano)
{
	
	unsigned int j;

	cout << "Las cartas que posee son: " << endl;
		
	for(j = 0;j < mano.size(); j++)
	{
		//retornar elementos de la carta num y palo		
		cout << "oli" << endl;

	}


}


string verificar_decision(string entrada)
{

	while(entrada.compare("p") != 0 && entrada.compare("pc") != 0)
	{

		cout << "Ingrese una de las opciones validas que se le han propuesto" << endl;
		cout << "Debe decidir que hacer, escriba 'p' si se quiere plantar, escriba 'pc' si quiere pedir cartas" << endl;
		cin >> entrada;		

	}

	return entrada;

}
