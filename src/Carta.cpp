#include "Carta.hpp"
#include <cstdlib>
#include <iostream>
#include <sstream>
using namespace std;



Carta::Carta(int numero, Palo palo)
{
    this->numero = numero;
    this->palo = palo;
}

Carta::Carta()
{
    int carta = (random() % 52) + 1;

    this->numero = (carta % 13) + 1;

    if(carta < 14)
    {
        this->palo = Palo::Pica;
    }else if(carta < 14 + 13){
        this->palo = Palo::Corazones;
    }else if(carta < 14 + 13 * 2){
        this->palo = Palo::Diamantes;
    }else if(carta < 14 + 13 * 3){
        this->palo = Palo::Treboles;
    }else{
        cerr << "ERROR AL CALCULAR UNA CARTA ALEATORIA: " << carta << endl;
    }
}


string Carta::toString() const
{
    stringstream s;
    s << "Carta: ";


    switch(this->palo)
    {
        case Palo::Pica:
            s << "pica:";
            break;
        case Palo::Corazones:
            s << "corazones:";
            break;
        case Palo::Diamantes:
            s << "diamantes:";
            break;
        case Palo::Treboles:
            s << "treboles:";
            break;

        default:
            s << "palo no valido";
            break;
    }


    s << this->numero;
    return s.str();
}



bool Carta::operator==(const Carta &c)
{
    return c.numero == this->numero and c.palo == this->palo;
}


string Carta::serializar() const
{
    vector<int> numeros;

    numeros.push_back(this->numero);


    switch(this->palo)
    {
        case Palo::Pica:
            numeros.push_back(0);
            break;
        case Palo::Corazones:
            numeros.push_back(1);
            break;
        case Palo::Diamantes:
            numeros.push_back(2);
            break;
        case Palo::Treboles:
            numeros.push_back(3);
            break;
    }


    return this->toFormat(numeros);
}


void Carta::desserializar(const string &s)
{
    vector<string> vec = this->parser(s);

    this->numero = stoi(vec.at(0));

    switch(stoi(vec.at(1)))
    {
        case 0:
            this->palo = Palo::Pica;
            break;

        case 1:
            this->palo = Palo::Corazones;
            break;

        case 2:
            this->palo = Palo::Diamantes;
            break;

        case 3:
            this->palo = Palo::Treboles;
            break;

    }
}

