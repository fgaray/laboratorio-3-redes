#ifndef _H_ESTADO_
#define _H_ESTADO_

#include <iostream>
#include <vector>
using namespace std;


#include "Carta.hpp"
#include "Comunicacion.hpp"

#include <pthread.h> 


using Jugador = unsigned int;


class Estado{

    private:

        vector<unsigned int> puntajes;
        vector<unsigned int> fichas;
        vector<bool> apostado;
        vector<Carta> cartas_generadas;
        vector<vector<Carta>> cartas;
        vector<Comunicacion> conexiones;
        vector<bool> plantado;
        vector<bool> respuestas;

        vector<Carta> cartas_banca;

        int pozo;

        bool cartas_enviadas;

        bool todos_listos;

        size_t jugadores;

        pthread_mutex_t bloquear;

        pthread_cond_t esperar_jugadores;
        pthread_cond_t esperar_apuestas;
        pthread_cond_t esperar_plantados;
        pthread_cond_t esperar_jugadores_listos;


        void block();

        void unblock();

    public:

        Estado();

        Jugador agregarJugador(int socket);


        int getFichas(Jugador jugador);


        bool apostar(Jugador jugador, int numero);

        Carta generarCarta(bool exclusion = false);


        bool checkApostado(Jugador jugador);






        vector<unsigned int> getPuntajes();

        bool hay21();

        Jugador ganador();


        void esperarJugadores();
        void esperarJugadoresListos();


        void repartirCartasIniciales();
        void repartirCarta();


        void esperarApuestas();

        void plantar(Jugador j);


        void enviarCartasIniciales();


        void enviarUltimaCarta();


        bool todosPlantados();


        int pozoTotal();
        

        void broadcast(int cod);


        bool cartasEnviadas();


        void siguienteRonda();

        void enviarResultado();


        void notificarRespuesta(Jugador j);


        bool todosListos();

        void sacarCartaBanca();
        int puntajeCartas(const vector<Carta> &cartas);
};


#endif /* end of include guard: _H_ESTADO_ */
