
#define PORT 8888



#define MAX_CLIENTES 2



// codigos para poder comunicarnos entre clientes


#define TAMANO_BYTES_CODIGOS 4



// OK


#define CONEXION_ACEPTADA 200

#define CERRAR_CONEXION 201

#define APUESTA_OK 202


#define CODIGO_PLANTARSE 203


#define CODIGO_NO_PLANTARSE 204

#define CONTINUAR 205


// preguntas

#define PREGUNTAR_APUESTAS 300


#define PEDIR_PLANTAR 301


#define ENVIAR_CARTAS 302

#define ENVIAR_CARTAS_INICIALES 303


// Errores

#define ERROR_MAXIMO_CLIENTES 500


#define ERROR_APUESTA_INVALIDA 501
