#include "Comunicacion.hpp"

#include <errno.h>
#include <string.h>



Comunicacion::Comunicacion(int socket)
{
    this->socket = socket;
}


#include <iostream>

void Comunicacion::enviar(int cod)
{
    cout << "[DEBUG] Enviando " << cod << endl;
    if(write(this->socket, &cod, sizeof(int)) != 4)
    {
        cout << "Error enviando: " << strerror(errno) << endl;
    }
}



int Comunicacion::recibirInt()
{
    int msg = 0;

    while(read(this->socket, &msg, sizeof(msg)) != 4)
    {

    }

    return msg;
}


void Comunicacion::enviarCodigoConexionAceptada()
{
    this->enviar(CONEXION_ACEPTADA);
}


void Comunicacion::enviarCodigoErrorMaximoClientes()
{
    this->enviar(ERROR_MAXIMO_CLIENTES);
}



void Comunicacion::enviarCodigoTerminar()
{
    this->enviar(CERRAR_CONEXION);
}


void Comunicacion::enviar(const string &s)
{
    const char *cadena = s.c_str();

    write(this->socket, cadena, strlen(cadena) + 1);
}




void Comunicacion::enviar(const Carta &s)
{
    this->enviar(s.numero);

    switch(s.palo)
    {
        case Palo::Pica:
            this->enviar(0);
            break;
        case Palo::Corazones:
            this->enviar(1);
            break;
        case Palo::Diamantes:
            this->enviar(2);
            break;
        case Palo::Treboles:
            this->enviar(3);
            break;
    }

}


string Comunicacion::recibirString()
{
    char c = ' ';

    string s;

    while(c != '\0')
    {
        read(this->socket, &c, 1);
        
        s.push_back(c);
    }


    return s;
}


Carta Comunicacion::recibirCarta()
{
    int numero = recibirInt();
    int palo = recibirInt();

    switch(palo)
    {
        case 0:
            return Carta(numero, Palo::Pica);
            break;
        case 1:
            return Carta(numero, Palo::Corazones);
            break;
        case 2:
            return Carta(numero, Palo::Diamantes);
            break;
        case 3:
            return Carta(numero, Palo::Treboles);
            break;

    }
        
}




void Comunicacion::enviarCodigoErrorApuesta()
{
    this->enviar(ERROR_APUESTA_INVALIDA);
}



void Comunicacion::enviarCodigoPreguntarApuestas()
{
    this->enviar(PREGUNTAR_APUESTAS);
}



void Comunicacion::enviarCodigoConfirmarApuesta()
{
    this->enviar(APUESTA_OK);
}



void Comunicacion::enviarCodigoPerdirPlantar()
{
    this->enviar(PEDIR_PLANTAR);
}
