#include "Estado.hpp"

#include <algorithm>
#include "config.hpp"


Estado::Estado()
{
    pthread_mutex_init(&(this->bloquear), nullptr);

    pthread_cond_init(&(this->esperar_jugadores), nullptr);
    pthread_cond_init(&(this->esperar_apuestas), nullptr);
    pthread_cond_init(&(this->esperar_plantados), nullptr);
    pthread_cond_init(&(this->esperar_jugadores_listos), nullptr);

    this->pozo = 0;

    this->jugadores = 0;
}


Jugador Estado::agregarJugador(int socket)
{
    this->jugadores++;

    this->puntajes.push_back(0);
    this->fichas.push_back(100);
    this->apostado.push_back(false);
    this->plantado.push_back(false);
    this->respuestas.push_back(false);

    vector<Carta> v;

    this->cartas.push_back(v);

    this->conexiones.push_back(Comunicacion(socket));


    if(this->jugadores == MAX_CLIENTES)
    {
        pthread_cond_signal(&(this->esperar_jugadores));
    }

    return (this->jugadores - 1);
}


int Estado::getFichas(Jugador jugador)
{
    return this->fichas.at(jugador);
}


bool Estado::apostar(Jugador jugador, int numero)
{
    this->block();

    if(fichas.at(jugador) >= numero)
    {

        this->fichas.at(jugador) = this->fichas.at(jugador) - numero;
        this->pozo += numero;
        this->apostado.at(jugador) = true;

        bool val = true;

        for(size_t i = 0; i < this->jugadores; i++)
        {
            val = val && this->checkApostado(i);
        }

        if(val)
        {
            //todos apostaron, asi que podemos desbloquear 

            pthread_cond_signal(&(this->esperar_apuestas));
        }


        return true;
    }else{
        return false;
    }


    this->unblock();
}


bool Estado::checkApostado(Jugador jugador)
{
    return this->apostado.at(jugador);
}


Carta Estado::generarCarta(bool exclusion)
{
    if(!exclusion)
    {
        this->block();
    }

    unsigned int iters = 0;

    Carta carta;

    //while(find(this->cartas_generadas.begin(), this->cartas_generadas.end(), carta) == this->cartas_generadas.end())
    //{
        //cout << "generando" << endl;
        //Carta c1;
        //carta = c1;
    //}

    if(!exclusion)
    {
        this->unblock();
    }

    return carta;
}


vector<unsigned int> Estado::getPuntajes()
{
    return this->puntajes;
}


bool Estado::hay21()
{
    for(unsigned int n: this->puntajes)
    {
        if(n == 21)
        {
            return true;
        }
    }

    return false;
}



Jugador Estado::ganador()
{
    unsigned int ganador;
    int cercano = 21;

    for(unsigned int i = 0; i < this->puntajes.size(); i++)
    {
        if((21 - this->puntajes.at(i)) < cercano)
        {
            cercano = 21 - this->puntajes.at(i);
            ganador = i;
        }
    }

    return ganador;
}


void Estado::esperarJugadores()
{
    pthread_mutex_lock(&(this->bloquear));

    pthread_cond_wait(&(this->esperar_jugadores), &(this->bloquear));

    pthread_mutex_unlock(&(this->bloquear));
}


void Estado::repartirCartasIniciales()
{
    this->block();

    for(size_t i = 0; i < this->jugadores; i++)
    {
        //agregamos las dos cartas iniciales
        this->cartas.at(i).push_back(this->generarCarta(true));
        this->cartas.at(i).push_back(this->generarCarta(true));
    }


    this->cartas_banca.push_back(this->generarCarta(true));
    this->cartas_banca.push_back(this->generarCarta(true));

    this->unblock();
}


void Estado::plantar(Jugador j)
{
   this->block(); 

   this->plantado.at(j) = true;

   this->respuestas.at(j) = true;

   if(todosPlantados())
   {
       pthread_cond_signal(&(this->esperar_plantados));
   }

   this->unblock(); 
}


bool Estado::todosPlantados()
{
   this->block(); 

   bool b = all_of(this->plantado.begin(), this->plantado.end(), [](bool b){ return b; });

   if(b)
   {
       pthread_cond_signal(&(this->esperar_plantados));
   }

   this->unblock(); 

   return b;
}



void Estado::block()
{
    pthread_mutex_unlock(&(this->bloquear));
}



void Estado::unblock()
{
    pthread_mutex_unlock(&(this->bloquear));
}



void Estado::esperarApuestas()
{
    pthread_mutex_lock(&(this->bloquear));


    pthread_cond_wait(&(this->esperar_apuestas), &(this->bloquear));

    pthread_mutex_unlock(&(this->bloquear));
}


void Estado::esperarJugadoresListos()
{
    pthread_mutex_lock(&(this->bloquear));

    pthread_cond_wait(&(this->esperar_jugadores_listos), &(this->bloquear));

    pthread_mutex_unlock(&(this->bloquear));
}


void Estado::repartirCarta()
{
    this->block();

    //hay que tener cuidado de repartir cartas solo a quellos jugadores que no
    //esten plantados

    for(size_t i = 0; i < this->jugadores; i++)
    {
        if(!(this->plantado.at(i)))
        {
            this->cartas.at(i).push_back(this->generarCarta(true));
        }
    }

    this->cartas_enviadas = true;

    this->unblock();
}


int Estado::pozoTotal()
{
    return this->pozo;
}



void Estado::enviarCartasIniciales()
{
    this->block();

    for(size_t i = 0; i < this->jugadores; i++)
    {
        for(auto c: this->cartas.at(i))
        {
            this->conexiones.at(i).enviar(c);
        }
    }

    this->cartas_enviadas = true;

    this->unblock();
}



void Estado::enviarUltimaCarta()
{
    this->block();

    for(size_t i = 0; i < this->jugadores; i++)
    {
        if(!this->plantado.at(i))
        {
            cout << "Enviando " << endl;
            this->conexiones.at(i).enviar(this->cartas.at(i).at(this->cartas.size() - 1));
        }
    }

    this->cartas_enviadas = true;

    this->unblock();
}

void Estado::broadcast(int cod)
{
    this->block();

    for(size_t i = 0; i < this->jugadores; i++)
    {

        if(!this->plantado.at(i))
        {
            this->conexiones.at(i).enviar(cod);
        }
    }

    this->unblock();
}



bool Estado::cartasEnviadas()
{
    this->block();

    bool b = this->cartas_enviadas;

    this->unblock();

    return b;
}


void Estado::siguienteRonda()
{
    this->block();

    this->cartas_enviadas = false;
    this->todos_listos = false;

    cout << "[DEBUG] Siguiente ronda" << endl;

    for(size_t i = 0; i < this->jugadores; i++)
    {
        this->apostado.at(i) = false;
        this->respuestas.at(i) = false;
    }

    this->unblock();
}

void Estado::notificarRespuesta(Jugador j)
{
    this->block();
    this->respuestas.at(j) = true;
    this->unblock();
}

bool Estado::todosListos()
{
    this->block();

    bool b = all_of(this->respuestas.begin(), this->respuestas.end(), [](bool b){ return b; });

    this->unblock();

    return b;
}



void Estado::sacarCartaBanca()
{
    this->block();

    if(puntajeCartas(this->cartas_banca))
    {
        this->cartas_banca.push_back(this->generarCarta(true));
    }

    this->unblock();
}


int Estado::puntajeCartas(const vector<Carta> &cartas)
{
    int puntaje = 0;
    
    for(auto c: cartas)
    {
        puntaje += c.numero;
    }

    return puntaje;
}
